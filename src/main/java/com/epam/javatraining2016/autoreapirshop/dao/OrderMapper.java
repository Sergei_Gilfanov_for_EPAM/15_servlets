package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.List;

public interface OrderMapper {
  void insert(OrderRow order);

  OrderRow selectShallow(int orderId);

  List<OrderRow> readList(OrderListRow orderList);

}
