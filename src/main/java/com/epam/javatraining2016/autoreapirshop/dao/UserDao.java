package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.NoSuchElementException;

import org.apache.ibatis.session.SqlSession;

public class UserDao {
  private UserMapper mapper;

  public UserDao(SqlSession sqlSession) {
    mapper = sqlSession.getMapper(UserMapper.class);
  }

  public UserRow create(String login, String name, String password) {
    UserRow retval = new UserRow(0);
    retval.setLogin(login);
    retval.setFullName(name);
    retval.setPassword(password);
    mapper.insert(retval);
    return retval;
  }

  public UserRow get(int userId) {
    UserRow retval = mapper.select(userId);
    if (retval == null) {
      throw new NoSuchElementException();
    }
    return retval;
  }
  
  public UserRow findByLogin(String login) {
    UserRow retval = mapper.selectByLogin(login);
    return retval;
  }
}
