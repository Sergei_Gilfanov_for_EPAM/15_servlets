package com.epam.javatraining2016.autoreapirshop.dao;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

public class AutoRepairShopServiceDao implements AutoCloseable {
  // private static final Logger log = LoggerFactory.getLogger(AutoRepairShopServiceDao.class);

  private SqlSession sqlSession;

  public AutoRepairShopServiceDao(SqlSessionFactory sqlSessionFactory) {
    this.sqlSession = sqlSessionFactory.openSession();
  }

  public void close() {
    sqlSession.rollback();
    sqlSession.close();
  }

  public void commit() {
    sqlSession.commit();
  }

  public OrderHistoryDao orderHistory() {
    return new OrderHistoryDao(sqlSession);
  }

  public InvoiceListDao invoiceList() {
    return new InvoiceListDao(sqlSession);
  }

  public OrderHistoryRecordDao orderHistoryRecord() {
    return new OrderHistoryRecordDao(sqlSession);
  }

  public OrderListDao orderList() {
    return new OrderListDao(sqlSession);
  }

  public OrderDao order() {
    return new OrderDao(sqlSession);
  }

  public OrderStatusDao orderStatus() {
    return new OrderStatusDao(sqlSession);
  }

  public CommentDao comment() {
    return new CommentDao(sqlSession);
  }

  public ClientDao client() {
    return new ClientDao(sqlSession);
  }

  public OrderHistoryRecordStatusChangeDao orderHistoryRecordStatusChange() {
    return new OrderHistoryRecordStatusChangeDao(sqlSession);
  }

  public OrderHistoryRecordCommentDao orderHistoryRecordComment() {
    return new OrderHistoryRecordCommentDao(sqlSession);
  }
  
  public UserDao user() {
    return new UserDao(sqlSession);
  }
}
