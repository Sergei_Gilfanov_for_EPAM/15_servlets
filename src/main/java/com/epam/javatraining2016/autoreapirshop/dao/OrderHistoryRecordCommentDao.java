package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.NoSuchElementException;

import org.apache.ibatis.session.SqlSession;

public class OrderHistoryRecordCommentDao {
  private OrderHistoryRecordMapper parentMapper;
  private OrderHistoryRecordCommentMapper mapper;

  public OrderHistoryRecordCommentDao(SqlSession sqlSession) {
    parentMapper = sqlSession.getMapper(OrderHistoryRecordMapper.class);
    mapper = sqlSession.getMapper(OrderHistoryRecordCommentMapper.class);
  }

  public OrderHistoryRecordCommentRow create(OrderHistoryRow orderHistory, CommentRow comment) {
    OrderHistoryRecordCommentRow retval = new OrderHistoryRecordCommentRow(0);
    retval.setOrderHistory(orderHistory); // Parent (OrderHistoryRecord) property
    retval.setComment(comment);

    parentMapper.insert(retval);
    mapper.insert(retval);
    return retval;
  }

  public OrderHistoryRecordCommentRow getShallow(int recordId) {
    OrderHistoryRecordCommentRow retval = mapper.selectShallow(recordId);
    if (retval == null) {
      throw new NoSuchElementException();
    }
    return retval;
  }
}
