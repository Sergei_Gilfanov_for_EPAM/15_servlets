package com.epam.javatraining2016.autoreapirshop.dao;

public class UserRow extends Row {
  private String login = null;
  private String fullName = null;
  private String password = null;

  public UserRow(Integer id) {
    super(id);
  }
  
  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String name) {
    this.fullName = name;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}
