package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.NoSuchElementException;

import org.apache.ibatis.session.SqlSession;

public class OrderStatusDao {
  private OrderStatusMapper mapper;

  public OrderStatusDao(SqlSession sqlSession) {
    mapper = sqlSession.getMapper(OrderStatusMapper.class);
  }

  public OrderStatusRow get(int orderStatusId) {
    OrderStatusRow retval = mapper.select(orderStatusId);
    if (retval == null) {
      throw new NoSuchElementException();
    }
    return retval;
  }

  public OrderStatusRow findByName(String statusName) {
    OrderStatusRow retval = mapper.selectByName(statusName);
    return retval;
  }

  public OrderStatusRow getCurrentStatus(OrderRow order) {
    OrderStatusRow retval = mapper.selectCurrentStatus(order.getOrderHistory());
    if (retval == null) {
      // Не должно происходит - мы при создании заказа создаем одну запись истории
      throw new NoSuchElementException();
    }
    return retval;
  }
}
