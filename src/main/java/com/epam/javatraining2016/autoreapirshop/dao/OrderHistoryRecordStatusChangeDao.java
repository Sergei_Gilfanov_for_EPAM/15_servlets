package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.NoSuchElementException;

import org.apache.ibatis.session.SqlSession;

public class OrderHistoryRecordStatusChangeDao {
  private OrderHistoryRecordMapper parentMapper;
  private OrderHistoryRecordStatusChangeMapper mapper;

  public OrderHistoryRecordStatusChangeDao(SqlSession sqlSession) {
    parentMapper = sqlSession.getMapper(OrderHistoryRecordMapper.class);
    mapper = sqlSession.getMapper(OrderHistoryRecordStatusChangeMapper.class);
  }

  public OrderHistoryRecordStatusChangeRow create(OrderHistoryRow orderHistory,
      OrderStatusRow orderStatus, CommentRow comment) {
    OrderHistoryRecordStatusChangeRow retval = new OrderHistoryRecordStatusChangeRow(0);
    retval.setOrderHistory(orderHistory); // Parent (OrderHistoryRecord) property
    retval.setOrderStatus(orderStatus);
    retval.setComment(comment);

    parentMapper.insert(retval);
    mapper.insert(retval);
    return retval;
  }

  public OrderHistoryRecordStatusChangeRow getShallow(int recordId) {
    OrderHistoryRecordStatusChangeRow retval = mapper.selectShallow(recordId);
    if (retval == null) {
      throw new NoSuchElementException();
    }
    return retval;
  }
}
