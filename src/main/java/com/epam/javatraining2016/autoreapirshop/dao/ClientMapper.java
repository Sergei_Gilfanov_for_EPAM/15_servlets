package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.List;

public interface ClientMapper {
  void insert(ClientRow client);

  ClientRow selectShallow(int clientId);

  List<ClientRow> selectAll();
}
