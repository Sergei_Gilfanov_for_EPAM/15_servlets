package com.epam.javatraining2016.autoreapirshop.dao;

public interface OrderStatusMapper {
  OrderStatusRow select(int orderStatusId);

  OrderStatusRow selectByName(String statusName);

  OrderStatusRow selectCurrentStatus(OrderHistoryRow orderHistory);
}
