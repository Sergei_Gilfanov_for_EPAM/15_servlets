package com.epam.javatraining2016.servlets;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.javatraining2016.autoreapirshop.dao.AutoRepairShopServiceDao;

public class HttpServletWithDao extends HttpServlet {
  private static final long serialVersionUID = -7753969284387848206L;
  protected AutoRepairShopServiceDao dao;

  @Override
  public void init() throws ServletException {
    super.init();
    ServletContext context = getServletContext();
    dao = (AutoRepairShopServiceDao) context.getAttribute("dao");
    if (dao == null) {
      throw new UnavailableException(
          "Dao object was not initialised. Search logs for exceptions rised from InitializeResources");
    }
  }

  protected void setCommonAttributes(HttpServletRequest request) {
    HttpSession session = request.getSession();
    Object user = session.getAttribute("user");
    if (user != null) {
      request.setAttribute("user", user);
    }

    String url = request.getRequestURL().toString();
    String baseURL = url.substring(0, url.length() - request.getRequestURI().length())
        + request.getContextPath() + "/";
    request.setAttribute("baseUrl", baseURL);
  }

  protected boolean checkAuthRedirect(HttpServletRequest request, HttpServletResponse response) {
    boolean authorized = true;

    while (true) {
      HttpSession session = request.getSession();
      if ( session == null) {
        authorized = false;
        break;
      }
      
      Object user = session.getAttribute("user");
      if (user == null) {
        authorized = false;
        break;
      }
      break;
    }
    
    if (!authorized) {
      response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
      response.setHeader("Location", request.getContextPath());
    }
    return !authorized;
  }
  
}
