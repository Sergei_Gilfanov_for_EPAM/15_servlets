package com.epam.javatraining2016.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/logout")
public class Logout extends HttpServletWithDao {
  private static final long serialVersionUID = -6104543282872441458L;

  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    setCommonAttributes(request);
    HttpSession session = request.getSession(false);
    if (session != null) {
      session.invalidate();
    }
    response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
    response.setHeader("Location", request.getContextPath());
  }
}
