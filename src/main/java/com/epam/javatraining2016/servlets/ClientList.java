package com.epam.javatraining2016.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.epam.javatraining2016.autoreapirshop.dao.ClientRow;

@WebServlet("/clients/")
public class ClientList extends HttpServletWithDao {
  private static final long serialVersionUID = 7418693864058602387L;

  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    if (checkAuthRedirect(request, response)) {
      return;
    }
    setCommonAttributes(request);

    List<ClientRow> clients = dao.client().getAll();
    request.setAttribute("clients", clients);
    request.getRequestDispatcher("/WEB-INF/templates/clients.jsp").forward(request, response);
  }
}
