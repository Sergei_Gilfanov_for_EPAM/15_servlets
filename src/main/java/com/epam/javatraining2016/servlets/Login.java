package com.epam.javatraining2016.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.javatraining2016.autoreapirshop.dao.UserDao;
import com.epam.javatraining2016.autoreapirshop.dao.UserRow;

@WebServlet({"", "/login"})
public class Login extends HttpServletWithDao {
  private static final long serialVersionUID = 7458074456678150192L;

  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    setCommonAttributes(request);
    request.getRequestDispatcher("/WEB-INF/templates/login.jsp").forward(request, response);
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO: проверки и сообщения об ошибках

    String login = request.getParameter("login");
    String password = request.getParameter("password");
    UserDao userDao = dao.user();
    UserRow user = userDao.findByLogin(login);

    boolean userNotFound = false;

    if (user == null || !password.equals(user.getPassword())) {
      userNotFound = true;
    }

    if (userNotFound) {
      request.setAttribute("userNotFound", true);
      request.getRequestDispatcher("/WEB-INF/templates/login.jsp").forward(request, response);
      return;
    }
    user.setPassword(null);

    HttpSession session = request.getSession(true);
    session.setAttribute("user", user);
    response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
    /*
     * Нам не нужно возвращать абсолютный путь - относительные URI в Location разрешены текущим
     * вариантом HTTP стандарта.
     */
    response.setHeader("Location", request.getContextPath() + "/clients/");
  }

}
