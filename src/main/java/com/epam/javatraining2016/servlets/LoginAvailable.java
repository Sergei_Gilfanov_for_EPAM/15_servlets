package com.epam.javatraining2016.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.epam.javatraining2016.autoreapirshop.dao.UserDao;
import com.epam.javatraining2016.autoreapirshop.dao.UserRow;

@WebServlet("/backend/loginAvailable.jsp")
public class LoginAvailable extends HttpServletWithDao {
  private static final long serialVersionUID = 7458074456678150192L;

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    PrintWriter out = response.getWriter();
    response.setContentType("application/json; charset=utf-8");
    
    String login = request.getParameter("login");
    if (login == null) {
      response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
      out.println("{reason: 'no login'}");
      return;
    }
    
    UserDao userDao = dao.user();
    UserRow user = userDao.findByLogin(login);

    if (user == null ) {
      response.setStatus(HttpServletResponse.SC_OK);
      out.println("{}");
      return;
    }
    
    response.setStatus(HttpServletResponse.SC_CONFLICT);
    out.println("{reason: 'already used'}");
    return;
  }

}
