package com.epam.javatraining2016.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.ibatis.session.SqlSessionFactory;

import com.epam.javatraining2016.autoreapirshop.dao.UserDao;
import com.epam.javatraining2016.autoreapirshop.dao.UserRow;

@WebServlet("/registration")
public class Registration extends HttpServletWithDao {
  private static final long serialVersionUID = 47858844927286199L;

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    setCommonAttributes(request);
    request.getRequestDispatcher("/WEB-INF/templates/registration.jsp").forward(request, response);
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.setCharacterEncoding("UTF-8");
    PrintWriter out = response.getWriter();

    String login = request.getParameter("login");
    if (login == null || login.length() == 0) {
      response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
      out.println("{reason: 'no login'}");
      return;
    }

    String fullName = request.getParameter("fullName");
    if (fullName == null || fullName.length() == 0) {
      response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
      out.println("{reason: 'no fullName'}");
      return;
    }

    String password = request.getParameter("password");
    if (password == null || password.length() == 0) {
      response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
      out.println("{reason: 'no password'}");
      return;
    }

    String password2 = request.getParameter("password2");
    if (password2 == null || password2.length() == 0) {
      response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
      out.println("{reason: 'no password2'}");
      return;
    }

    if (!password.equals(password2)) {
      response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
      out.println("{reason: 'password != password2'}");
      return;
    }

    UserDao userDao = dao.user();
    UserRow user = userDao.create(login, fullName, password);
    dao.commit();
    HttpSession session = request.getSession(true);
    session.setAttribute("user", user);
    response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
    /*
     * Нам не нужно возвращать абсолютный путь - относительные URI в Location разрешены текущим
     * вариантом HTTP стандарта.
     */
    response.setHeader("Location", request.getContextPath() + "/clients/");
  }
}
