package com.epam.javatraining2016.context;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.epam.javatraining2016.autoreapirshop.dao.AutoRepairShopServiceDao;

/* Путь - фальшивый. Сервлет существует только для заполнения глобального
 * контекста веб-приложения. ServletContextListener не пользуемся потому, что
 * из его методов мы не можем выкинуть исключений.
 */
@WebServlet(name="InitializeResources", urlPatterns="/WEB-INF/initializeResources", loadOnStartup=1)
public class InitializeResources extends HttpServlet {
  private static final long serialVersionUID = 2308920946619591555L;
  
  final private static String jdbcPropertyFilePath = "/WEB-INF/jdbc.properties";
  
  @Override
  public void init() throws ServletException {
    super.init();
    ServletContext context = getServletContext();
    
    SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
    InputStream myBatisConfig =
        AutoRepairShopServiceDao.class.getResourceAsStream("mybatis-config.xml");

    Properties properties = new Properties();
    InputStream jdbcConfig = getServletContext().getResourceAsStream(jdbcPropertyFilePath);
    if (jdbcConfig == null) {
      throw new ServletException("Can't open jdbc config " + jdbcPropertyFilePath);
    }

    try {
      properties.load(jdbcConfig);
    } catch (IOException e) {
      throw new ServletException(e);
    }
    SqlSessionFactory sqlSessionFactory = builder.build(myBatisConfig, properties);
    AutoRepairShopServiceDao dao = new AutoRepairShopServiceDao(sqlSessionFactory);
    context.setAttribute("dao", dao);
    context.log("DAO created");
  }

  @Override
  public void destroy() {
    ServletContext context = getServletContext();
    AutoRepairShopServiceDao dao = (AutoRepairShopServiceDao) context.getAttribute("dao");
    dao.close();
    super.destroy();
  }
  
}
