<!doctype html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<title></title>
	<meta name="description" content="">
	<meta name="author" content="">


	<link rel="stylesheet" href="css/palette.css">
	<link rel="stylesheet" href="css/styles-0.1.css">
</head>
<body>
	<div class="client-list-page app-page hidden">
		<h1>Список клиентов</h1>
		<p>
			<ul class="client-list">
				<li class="li-template client-list-item">
					<span class="client-name clickable-text"></span>
				</li>
				<li class="li-loading">
					<span class="loading-spinner"></span>
				</li>
			</ul>
			<div class="button-container"><div class="add-client-button button">Добавить клиента</div></div>
		</p>
	</div>

	<div class="client-page app-page hidden">
		<h1>Заказы клиента</h1>
		<p>
			<ul class="order-list">
				<li class="li-template order-list-item">
					<span class="order-number clickable-text"></span> <span class="order-status"></span>
				</li>
				<li class="li-loading">
					<span class="loading-spinner"></span>
				</li>
			</ul>
			<div class="button-container"><div class="add-order-button button">Новый заказ</div></div>
		</p>
	</div>

	<div class="popup-overlay hidden">
		<div class="client-add app-popup hidden">
			<span class="close-button"></span>
			<form>
				<label>Имя нового клиента: <input type="text" name="clientName"/></label>
				<div class="button-container"><div class="button">Сохранить</div></div>
			</form>
		</div>

		<div class="order-add app-popup hidden">
			<span class="close-button"></span>
			<form>
				<label>Комментарий к заказу: <br>
					<textarea name="comment"></textarea>
				</label>
				<div class="button-container"><div class="button">Сохранить</div></div>
			</form>
		</div>

		<div class="order-history-popup app-popup hidden">
			<div class="close-button"></div>
			<div class="popup-content">
				<h1>История заказа</h1>
					<ul class="order-history">
						<li class="li-template order-history-item order-history-item-comment">
							<p class="history-item-comment"></p>
						</li>

						<li class="li-template order-history-item order-history-item-status-change">
							<span class="secondary-text-color">Заказ изменил статус на <span class="history-item-status"></span></span>
							<p class="history-item-comment"></p>
						</li>
						<li class="li-loading">
							<span class="loading-spinner"></span>
						</li>
					</ul>
					<form>
						<fieldset>
							<p>
								<label>Статус заказа
									<select name="order-status">
										<option value="" selected>Оставить без изменения</option>
										<option value="waiting">waiting</option>
										<option value="pending">pending</option>
										<option value="in progress">in progress</option>
										<option value="redo">redo</option>
										<option value="done">done</option>
										<option value="delivered">delivered</option>
									</select>
								</label>
							</p>
							<p>
								<label>Комментарий к заказу:<br>
									<textarea name="comment"></textarea>
								</label>
							</p>
						</fieldset>
							<p>
								<div class="button-container"><div class="button">Сохранить</div></div>
							</p>
				</form>
			</div>
		</div>
	</div>
	<script data-main="js/app" src="js/vendor/require.js"></script>
</body>
</html>
