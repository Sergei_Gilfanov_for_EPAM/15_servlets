<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>

<!doctype html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<title></title>
	<meta name="description" content="">
	<meta name="author" content="">
	<base href="${baseUrl}">

	<link rel="stylesheet" href="css/palette.css">
	<link rel="stylesheet" href="css/styles-0.1.css">
</head>
<body>
	<div class="popup-overlay">
		<div class="user-registration app-popup">
			<form action="" method="post">
				<label><span class="label-text">Имя учетной записи:</span><input type="text" name="login"/></label>
				<br>
				<label><span class="label-text">Имя пользователя:</span><input type="text" name="fullName"/></label>
				<br>
				<label><span class="label-text">Пароль:</span><input type="password" name="password"/></label>
				<br>
				<label><span class="label-text">Пароль(повторно):</span><input type="password" name="password2"/></label>
				<br>
				<div class="form-error-area">
					<span class="hidden login-used">Имя пользователя уже занято</span>
					<span class="hidden error">При проверке логина произошла ошибка</span>
				</div>
				<div class="button-container"><div class="add-order-button button">Зарегистрироваться</div></div>
			</form>
		</div>
		<script data-main="js/app" src="js/vendor/require.js"></script>
</body>
</html>
