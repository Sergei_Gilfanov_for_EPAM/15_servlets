<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>

<!doctype html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<title></title>
	<meta name="description" content="">
	<meta name="author" content="">
	<base href="${baseUrl}">

	<link rel="stylesheet" href="css/palette.css">
	<link rel="stylesheet" href="css/styles-0.1.css">
</head>
<body>
	<div class="popup-overlay">
		<div class="user-login app-popup">
			<form action="" method="post">
				<c:if test="${userNotFound}">
					<p>Пользователь не найден</p>
				</c:if>			
				<label>Имя учетной записи: <input type="text" name="login"/></label>
				<br>
				<label>Пароль: <input type="password" name="password"/></label>
				<div class="button-container"><button class="button" type="submit">Вход</button></div>
				<div><a href="registration">Регистрация</a></div>
			</form>
		</div>
</body>
</html>
