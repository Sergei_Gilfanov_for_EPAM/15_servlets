<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>

<!doctype html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<title></title>
	<meta name="description" content="">
	<meta name="author" content="">
	<base href="${baseUrl}">

	<link rel="stylesheet" href="css/palette.css">
	<link rel="stylesheet" href="css/styles-0.1.css">
</head>
<body>
	<div class="page-header">
		<span class="page-header_left"></span>
		<span class="page-header_right"><span>${user.fullName}</span>
		<a href="logout" class="page-header_logout-button">Выход</a></span>
	</div>
	<div class="client-list-page app-page">
		<h1>Список клиентов</h1>
		<p>
			<ul class="client-list">
			<c:forEach var="client" items="${clients}">
				<li class="client-list-item">
					<span class="client-name clickable-text"><c:out value="${client.clientName}"/></span>
				</li>
			</ul>
			</c:forEach>
			<div class="button-container"><div class="add-client-button button">Добавить клиента</div></div>
		</p>
	</div>

	<div class="popup-overlay hidden">
		<div class="client-add app-popup hidden">
			<span class="close-button"></span>
			<form>
				<label>Имя нового клиента: <input type="text" name="clientName"/></label>
				<div class="button-container"><div class="button">Сохранить</div></div>
			</form>
		</div>
	</div>
	<script data-main="js/app" src="js/vendor/require.js"></script>
</body>
</html>
