define(["jquery", "app/dao-0.1", "app/pages-0.1"], function ($, dao, pages) {
	$(function() {
		pages.clientList.show(dao.clientList);
	});
});
