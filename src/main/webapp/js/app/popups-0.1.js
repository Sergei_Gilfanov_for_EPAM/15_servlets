define(["jquery", "app/dao-0.1"], function ($, dao) {
	var overlayElement = $('.popup-overlay');

	var clientAdd = (function() {
		var inputDone = null;

		var popupElement = $('.client-add');
		var formElement = popupElement.find('form');

		popupElement.on( 'click', '.close-button', function() {
			hide();
			inputDone.reject();
		});

		formElement.find('.button').click( function(event) {
			event.preventDefault();
			dao.client.create(formElement.serialize())
			.done(function(data) {
				hide();
				console.log(data);
				inputDone.resolve(data);
			});
		});

		function show() {
			inputDone = $.Deferred();
			popupElement.show();
			overlayElement.show();
			return inputDone;
		}

		function hide() {
			overlayElement.hide();
			popupElement.hide();
		}

		return {
			'show': show
		}
	})();

	var orderAdd = (function() {
		var client;
		var inputDone = null;

		var popupElement = $('.order-add');
		var formElement = popupElement.find('form');
		var commentElement = formElement.find('textarea[name="comment"]');

		popupElement.on( 'click', '.close-button', function() {
			hide();
			inputDone.reject();
		});

		formElement.find('.button').click( function(event) {
			event.preventDefault();
			var orderCreated = dao.order.create(client);

			var messageAdded = orderCreated.then( function(order) {
				return dao.order.addMessage(order, commentElement.val());
			});

			var statusRead = orderCreated.then( function (order) {
				return dao.order.get(order);
			});

			$.when(statusRead, messageAdded)
			.then(function(orderAjaxRes, messageAjaxRes) {
				hide();
				inputDone.resolve(orderAjaxRes[0]);
			});
		});

		function show(aClient) {
			client = aClient;
			inputDone = $.Deferred();
			popupElement.show();
			overlayElement.show();
			return inputDone;
		}

		function hide() {
			overlayElement.hide();
			popupElement.hide();
		}

		return {
			'show': show
		}
	})();

	var orderHistory = (function() {
		var inputDone = null;
		var popupElement = $('.order-history-popup');
		var orderHistoryElement = $('.order-history');
		var historyItemTemplateComment = orderHistoryElement.find('.li-template.order-history-item-comment').detach();
		historyItemTemplateComment.removeClass('li-template');
		var historyItemTemplateStatusChange = orderHistoryElement.find('.li-template.order-history-item-status-change').detach();
		historyItemTemplateStatusChange.removeClass('li-template');

		var formElement = popupElement.find('form');
		var statusInput = formElement.find('select[name=order-status]');
		var commentInput = formElement.find('textarea[name=comment]');

		var orderHistoryLoadigElement = orderHistoryElement.find('.li-loading');

		var order = null;
		var orderChanged = false;

		function changeStatus(historyItem) {
			return $.post ( backendBase + "/changeStatus.py", { orderId: order.id, 'status': historyItem.status, comment: historyItem.comment } );
		}

		function cleanInputs() {
			commentInput.val("");
			statusInput.val("");
		}

		popupElement.on( 'click', '.close-button', function() {
			hide();
			if (!orderChanged) {
				inputDone.reject();
			} else {
				inputDone.resolve(dao.orderHistory.getCurrentStatus());
			}
		});


		formElement.find('.button').click( function(event) {
			event.preventDefault();
			var historyItem = null;
			var commandFn = null;
			switch( statusInput.val() ) {
				case '':
					historyItem = { '_type': "comment", comment: commentInput.val() };
					commandFn = dao.orderHistory.addComment;
					break;
				default:
					historyItem = { '_type': "statusChange", 'status':  statusInput.val(), comment:  commentInput.val() };
					commandFn = dao.orderHistory.changeStatus;
					break;
			}

			commandFn.call(dao.orderHistory, order, historyItem).done( function(data) {
				orderChanged = true;
				var historyItemElement = createHistoryItemElement(historyItem);
				orderHistoryElement.append(historyItemElement);
				cleanInputs();
			});
		});

		function show(orderData) {
			order = orderData;
			inputDone = $.Deferred();
			orderHistoryLoadigElement.show();
			popupElement.show();
			overlayElement.show();

			dao.orderHistory.get(orderData, true)
			.done(function(orderHistory) {
				orderHistoryLoadigElement.hide();
				var toAppend = [];
				$.each(orderHistory, function(i, historyItem) {
					var historyItemElement = createHistoryItemElement(historyItem);
					toAppend.push(historyItemElement);
				});
				orderHistoryElement.append(toAppend);
			});
			return inputDone;
		}

		function hide() {
			overlayElement.hide();
			popupElement.hide();
			orderHistoryElement.find('.order-history-item').remove();
		}

		function createHistoryItemElement(historyItem) {
			var retval = null;
			switch (historyItem._type) {
				case 'statusChange':
					retval = historyItemTemplateStatusChange.clone();
					retval.find('.history-item-status').text(historyItem.status);
					retval.find('.history-item-comment').text(historyItem.comment);
					break;
				case 'comment':
					retval = historyItemTemplateComment.clone();
					retval.find('.history-item-comment').text(historyItem.comment);
					break;
			}
			return retval;
		}

		return {
			'show': show
		}
	})();

	var	userRegister = (function() {
		var inputDone = null;

		var popupElement = $('.user-registration');
		var formElement = popupElement.find('form');

		var $login = formElement.find('input[name=login]');
		var $password = formElement.find('input[name=password]');
		var $password2 = formElement.find('input[name=password2]');
		var $errorArea = formElement.find('.form-error-area');

		formElement.find('.button').click( function(event) {
			event.preventDefault();
			$errorArea.find('span').hide();
			var login = $login.val();
			if (login.length == 0 ) {
				return;
			}
			dao.login.isAvailable(login)
			.done(function() {
				formElement.submit();
			})
			.fail(function(data) {
				switch(data) {
					case 'used':
						$errorArea.find('.login-used').show();
						break;
					default:
						$errorArea.find('.error').show();
				}
			});
		});
	})();

	return {
		clientAdd: clientAdd,
		orderAdd: orderAdd,
		orderHistory: orderHistory
	};
});
