define(["jquery", "app/dao-0.1", "app/popups-0.1", "app/pages/client-0.1"], function ($, dao, popups, client) {
	var clientListPageElement = $('.client-list-page');
	var clientListElement = $('.client-list')
	var clientListItemTemplate = clientListElement.find('.li-template').detach();
	clientListItemTemplate.removeClass("li-template");
	var clientListLoadingElement = clientListElement.find('.li-loading');

	clientListElement.on( 'click', '.client-list-item', function() {
		var clientData = $(this).data().clientData;
		hide();
		client.show( clientData);
	});

	clientListPageElement.find('.add-client-button').click(function() {
		popups.clientAdd.show()
		.done(function(newClient) {
			var clientElement = createClientElement(newClient);
			clientListElement.append(clientElement);
		});
	});

	function show(clientListDao) {
		clientListLoadingElement.show();
		clientListPageElement.show();

		var showDone = $.Deferred();
		clientListDao.get(true)
		.done(function(clientList) {
			clientListLoadingElement.hide();
			var toAppend = [];
			$.each(clientList, function(i, client) {
				var clientElement = createClientElement(client);
				toAppend.push(clientElement);
			});
			clientListElement.append(toAppend);
			showDone.resolve();
		});
		return showDone;
	}

	function hide() {
		clientListPageElement.hide();
		clientListElement.find('.client-list-item').remove();
	}

	function createClientElement(client) {
		var clientElement = clientListItemTemplate.clone();
		clientElement.find('.client-name').text(client.clientName);
		clientElement.data('clientData', client);
		return clientElement;
	}

	return {
		'show': show
	}
});
