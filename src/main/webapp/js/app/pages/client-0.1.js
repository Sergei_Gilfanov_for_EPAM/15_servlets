define(["jquery", "app/dao-0.1", "app/popups-0.1"], function ($, dao, popups) {
	var clientPageElement = $('.client-page');
	var orderListElement = $('.order-list');
	var orderListItemTemplate = orderListElement.find('.li-template').detach();
	orderListItemTemplate.removeClass('li-template');
	var orderListLoadingElement = orderListElement.find('.li-loading');

	orderListElement.on( 'click', '.order-list-item', function() {
		var orderData = $(this).data().orderData;
		var orderElement = $(this);
		popups.orderHistory.show(orderData)
		.done( function (newStatus) {
			orderElement.data().orderData.status = newStatus;
			orderElement.find('.order-status').text(newStatus);
		});
	});

	clientPageElement.find('.add-order-button').click(function() {
		popups.orderAdd.show(client)
		.done(function(newOrder) {
			var orderElement = createOrderElement(newOrder);
			orderListElement.append(orderElement);
		});
	});

	function show(aClient) {
		client = aClient;
		orderListLoadingElement.show();
		clientPageElement.show();

		var showDone = $.Deferred();
		dao.orderList.get(aClient, true)
		.done(function(orderList) {
			orderListLoadingElement.hide();
			var toAppend = [];
			$.each(orderList, function(i, order) {
				var orderElement = createOrderElement(order);
				toAppend.push(orderElement);
			});
			orderListElement.append(toAppend);
			showDone.resolve();
		});
		return showDone;
	}

	function hide() {
		clientPageElement.hide();
		orderListElement.find('.order-list-item').remove();
	}

	function createOrderElement(order) {
		var orderElement = orderListItemTemplate.clone();
		orderElement.find('.order-number').text(order.id);
		orderElement.find('.order-status').text(order.status);
		orderElement.data('orderData', order);
		return orderElement;
	}

	return {
		'show': show
	}
});
