define(function () {
	function ClientList(backendBase) {
		this.backendBase = backendBase;
		this.clientListCache = null;
	}
	ClientList.prototype.get =  function (refresh) {
		var self = this;
		var getDone = $.Deferred();

		if ( !refresh && list != null ) {
			getDone.resolve(self.clientListCache);
			return getDone;
		}

		$.get( self.backendBase + "/clientList.jsp", {} )
		.done(function(data) {
			self.clientListCache = data;
			getDone.resolve(data);
		})
		.fail(function(jqXHR, textStatus, errorThrown) {
			getDone.reject();
		});
		return getDone;
	};

	// -----------
	function Login(backendBase) {
		this.backendBase = backendBase;
	}

	Login.prototype.isAvailable = function(login) {
		var self = this;
		var checkDone = $.Deferred();

		$.post(self.backendBase + "/loginAvailable.jsp", {login: login})
		.done(function(data) {
			checkDone.resolve();
		})
		.fail(function(jqXHR, textStatus, errorThrown) {
			if (jqXHR.status == 409) {
				checkDone.reject('used');
			} else {
				checkDone.reject('error');
			}

		});
		return checkDone;
	}

	// --------------
	function Client(backendBase) {
		this.backendBase = backendBase;
	}

	Client.prototype.create = function (serializedForm) {
			return $.post( this.backendBase + "/createClient.py", serializedForm);
	}

	// --------------
	function OrderList(backendBase) {
		this.backendBase = backendBase;
		this.orderListCache = null;
	}
	OrderList.prototype.get = function (client, refresh) {
		var self = this;
		var getDone = $.Deferred();

		if (!refresh && orderListCache != null ) {
			getDone.resolve(self.orderListCache);
			return getDone;
		}

		$.get( self.backendBase + "/getOrders.py", {clientId: client.id} )
		.done(function(data) {
			self.orderListCache = data;
			getDone.resolve(data);
		})
		.fail(function() {
			getDone.reject();
		});
		return getDone;
	};

	// --------------
	function Order(backendBase) {
		this.backendBase = backendBase;
	}
	Order.prototype.create = function (client) {
		return $.post( this.backendBase + "/createOrder.py", {clientId:client.id});
	}
	Order.prototype.get = function (order) {
		return $.post( this.backendBase + "/getOrder.py", {orderId:order.id});
	}
	Order.prototype.addMessage = function(order, messageText) {
		return $.post( this.backendBase + "/createComment.py", {orderId:order.id, messageText:messageText});
	}

	// ------------
	function OrderHistory(backendBase) {
		this.backendBase = backendBase;
		this. orderHistoryCache = null;
	}
	OrderHistory.prototype.get = function (order, refresh) {
		var self = this;
		var getDone = $.Deferred();

		if ( !refresh && orderHistoryCache != null ) {
			getDone.resolve(self.orderHistoryCache);
			return getDone;
		}

		$.get( this.backendBase + "/getOrderHistory.py", {orderId: order.id} )
		.done(function(data) {
			self.orderHistoryCache = data;
			getDone.resolve(data);
		})
		.fail(function() {
			getDone.reject();
		});
		return getDone;
	};

	OrderHistory.prototype.addComment = function (order, historyItem) {
		var self = this;
		var retval = $.post( this.backendBase + "/createComment.py", { orderId: order.id, comment: historyItem.comment });
		retval.done( function(data) {
			self.orderHistoryCache.push(historyItem);
		});
		return retval;
	};

	OrderHistory.prototype.changeStatus = function (order, historyItem) {
		var self = this;
		var retval = $.post ( this.backendBase + "/changeStatus.py", { orderId: order.id, 'status': historyItem.status, comment: historyItem.comment } );
		retval.done( function(data) {
				self.orderHistoryCache.push(historyItem);
		});
		return retval;
	}

	OrderHistory.prototype.getCurrentStatus = function () {
		var self = this;
		var retval = null;
		$.each(self.orderHistoryCache, function(i, historyItem) {
			if ( historyItem._type == 'statusChange' ) {
				retval = historyItem.status;
			}
		});
		return retval;
	}

	var backendBase = "backend";
	return {
			clientList: new ClientList(backendBase),
			client: new Client(backendBase),
			orderList: new OrderList(backendBase),
			order: new Order(backendBase),
			orderHistory: new OrderHistory(backendBase),
			login: new Login(backendBase)
	};
});
