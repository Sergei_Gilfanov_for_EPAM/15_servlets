create table Vendor(
	id serial primary key
);

create table OrderHistory(
	id serial primary key
);

create table InvoiceList (
	id serial primary key
);

create table PaymentAddress(
	id serial primary key,
	vendor_id integer references Vendor(id)
);

create table OrderHistoryRecord (
	id serial primary key,
	order_history_id integer references OrderHistory(id)
);

create table WorkList(
	id serial primary key
);

create table MaterialList(
	id serial primary key
);

create table OrderList(
	id serial primary key
);

create table numericTransaction(
	id serial primary key,
	amount numeric
);

create table Employee(
	id serial primary key,
	employee_name text
);

create table Invoice(
	id serial primary key,
	invoice_list_id integer references InvoiceList(id),
	payment_addres_id integer references PaymentAddress(id)
);

create table WorkInfo (
	id serial primary key,
	work_name text,
	cost numeric
);

create table Orders (
	id serial primary key,
	invoice_list_id integer references InvoiceList(id),
	order_history_id integer references OrderHistory(id)
);

create table OrderStatus(
	id smallserial primary key,
	status_name varchar(20)
);

insert into OrderStatus(id, status_name) values 
(1, 'new'), (2, 'waiting'), (3, 'pending'), (4, 'in progress'), 
(5, 'redo'), (6, 'done'), (7, 'delivered');

create table Comment(
	id serial primary key,
	comment_text text
);

create table MaterialUsage(
	id serial primary key
);

create table MaterialInfo(
	id serial primary key,
	material_desc text,
	cost numeric not null
);

create table OrderHistoryRecordWork (
	id integer primary key references OrderHistoryRecord(id),
	work_list_id integer references WorkList(id),
	material_list_id integer references MaterialList(id)
);

create table Client (
	id serial primary key,
	client_name text,
	order_list_id integer references OrderList(id) not null
);

create table numericTransferSalary(
	id integer primary key references numericTransaction(id),
	employee_id integer references Employee(id) 
);

create table numericTransactionMisc(
	id integer primary key references numericTransaction(id),
	reason text
);

create table numericTransactionInvoicePayment(
	id integer primary key references numericTransaction(id),
	invoice_id integer references Invoice(id)
);

create table WorkListRecord (
	id serial primary key,
	work_list_id integer references WorkList(id),
	work_info_id integer references WorkInfo(id),
	hours numeric
);

create table Solutions(
	id serial primary key,
	solution_name text,
	solution_notes text,
	material_list_id integer references MaterialList(id),
	work_list_id integer references WorkList(id)
);

create table OrderListOrders(
	order_list_id integer references OrderList(id),
	order_id integer references Orders(id),
	primary key (order_list_id, order_id)
);

create table OrderHistoryRecordWorkAssigment (
	id integer primary key references OrderHistoryRecord(id),
	work_list_id integer references WorkList(id),
	material_list_id integer references MaterialList(id)
);

create table OrderHistoryRecordStatusChange(
	id integer primary key references OrderHistoryRecord(id),
	status_id integer references OrderStatus(id),
	comment_id integer references Comment(id)	
);

create table OrderHistoryRecordComment(
	id integer primary key references OrderHistoryRecord(id),
	comment_id integer references Comment(id)
);

create table OrderHistoryRecordAssignEmployee (
	id integer primary key references OrderHistoryRecord(id),
	employee_id integer references Employee(id)
);

create table MaterialUsageMisc(
	id integer primary key references MaterialUsage(id),
	material_list_id integer references MaterialList(id)
);

create table MaterialUsageAdjust(
	id integer primary key references MaterialUsage(id),
	material_list_id integer references MaterialList(id)
);

create table MaterialRequest(
	id serial primary key,
	invoice_list_id integer references InvoiceList(id),
	material_list_id integer references MaterialList(id),
	vendor_id integer references Vendor(id)
);

create table MaterialListRecord(
	id serial primary key,
	list_id integer references MaterialList(id),
	material_info_id integer references MaterialInfo(id)
);

create table MaterialDelivery(
	id serial primary key,
	material_list_id integer references MaterialList(id)
);

create table MatereialUsageOrder(
	id integer primary key references MaterialUsage(id),
	used_for integer references OrderHistoryRecordWork(id)
);

create table Contact(
	id serial primary key,
	client_id integer references Client(id),
	contacts_text text
);

create table Users(
	id serial primary key,
	login text,
	full_name text,
	password text
);
